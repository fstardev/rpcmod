import { Process, Processor } from '@nestjs/bull';
import { Injectable, Logger } from '@nestjs/common';
import { Job } from 'bull';

@Processor('queue')
export class QueueProcessor {
    private readonly logger = new Logger(QueueProcessor.name);

    @Process('task')
        handleTranscode(job: Job) {
        this.logger.debug('Start task...');
        this.logger.debug(job.data);
        this.logger.debug('Task completed');
    }
}
