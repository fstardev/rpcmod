import { InjectQueue } from '@nestjs/bull';
import { Body, Controller, Post } from '@nestjs/common';
import { Queue } from 'bull';
import { QueueDto } from './dto/queue.dto';

@Controller('queue')
export class QueueController {
    constructor(@InjectQueue('queue') private readonly taskQueue: Queue) {}

    @Post()
    async task(@Body() body: QueueDto) {
      await this.taskQueue.add('task', {
        rpcId: 'SP_PROC',
      }, { delay: body.delay });
      return { status: 'ok'}
    }
}
