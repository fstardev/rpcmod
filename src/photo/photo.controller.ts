import { Body, Controller, Get, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { PhotoDto } from './dto/photo.dto';

@Controller('photo')
export class PhotoController {
    
    @Get()
    findAll(): string {
        return 'This action returns all photos';
    }

    @UseInterceptors(FileInterceptor('file', {
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, process.env.UPLOAD_DIR)
        },
        filename: (req, file, cb) => {
          const data = req.body as PhotoDto;
          cb(null, file.originalname)       
        },
      })
    }))
    @Post()
    async uploadFile(
      @Body() body: PhotoDto,
      @UploadedFile() file: Express.Multer.File,
    ) {
    
      return {
        file: file.filename
      };
    }
}
